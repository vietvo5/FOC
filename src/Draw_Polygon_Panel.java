import java.awt.Graphics;

import javax.swing.JPanel;

public class Draw_Polygon_Panel extends JPanel {

	private int[] x = null, y = null;

	public void setCor(int[] x, int[] y) {
		this.x = x;
		this.y = y;
	}

	public void paint(Graphics g) {
		super.paint(g);

		if (x != null && y != null) {
			for (int i = 0; i < x.length - 1; i++) {
				g.drawLine(x[i], y[i], x[i + 1], y[i + 1]);
			}
			
			g.drawLine(x[0], y[0], x[x.length - 1], y[y.length - 1]);
		}
	}

}
