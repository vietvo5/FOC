import java.io.File;


public class OfficeFilter extends javax.swing.filechooser.FileFilter {

		@Override
		public boolean accept(File fOpen) {
			if (fOpen.isDirectory()) {
				return true;
			}
			String fileName = fOpen.getName();
			if (fileName.endsWith(".rtf") || fileName.endsWith(".doc")
					|| fileName.endsWith(".txt")) {
				return true;
			}
			return false;
		}

		@Override
		public String getDescription() {
			return "*.doc,*.rtf,*.txt";
		}
}

