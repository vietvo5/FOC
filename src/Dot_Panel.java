import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Dot_Panel extends JPanel {

	private int xRobot = 0, yRobot = 0;
	private int xTarget = 0, yTarget = 0;
	Timer tmMoving;
	int d = 3;
	private Color cColor = Color.RED;
	private int s = 100;

	// constructor

	public int getS() {
		return s;
	}

	public void setS(int s) {
		this.s = s;
	}

	public Color getcColor() {
		return cColor;
	}

	public void setcColor(Color cColor) {
		this.cColor = cColor;
	}

	public Dot_Panel() {
		tmMoving = new Timer(500, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int dx = xRobot - xTarget;
				int dy = yRobot - yTarget;
				if (dx == 0 && dy == 0) {
					return;
				}
				int mx = d, my = d;
				if (Math.abs(dx) < d) {
					mx = Math.abs(dx);
				}
				if (Math.abs(dy) < d) {
					my = Math.abs(dy);
				}
				
				if (dx > 0) {
					mx = -mx;
				}
				if (dy > 0) {
					my = -my;
				}
				
				xRobot = xRobot + mx;
				yRobot = yRobot + my;
				repaint();
			}
		});
		tmMoving.start();
		
		
		this.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent arg0) {
				xTarget = arg0.getX();
				yTarget = arg0.getY();

			}
		});
	}

	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(getcColor());
		g.fillOval(xRobot - 10, yRobot - 10, 20, 20);
	}

}
