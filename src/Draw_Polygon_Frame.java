import javax.swing.JFrame;


public class Draw_Polygon_Frame extends JFrame {
	/* ALL COMPONENTS */
	Draw_Polygon_Panel Polygon = new Draw_Polygon_Panel();
	
	/* Variable */
	int xPoly[] = {150, 250, 325, 375, 450, 275, 100};
	int yPoly[] = {150, 100, 125, 225, 250, 375, 300};

	public Draw_Polygon_Frame(){
		setTitle("");
		setSize(600, 600);
		add("Center", Polygon);
		
		
		Polygon.setCor(xPoly, yPoly);
		Polygon.repaint();
		
		
	}
	
	public static void main(String[] args) {
		Draw_Polygon_Frame GUI = new Draw_Polygon_Frame();
		GUI.setDefaultCloseOperation(EXIT_ON_CLOSE);
		GUI.setVisible(true);
	}

}
