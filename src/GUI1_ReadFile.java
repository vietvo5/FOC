import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class GUI1_ReadFile extends JFrame {
	
	/* ALL COMPONENTS */
	JTextField txtInput = new JTextField();
	JButton btnBrowse = new JButton("Browse");
	JTextArea txtOutput = new JTextArea();
	JScrollPane Scroll = new JScrollPane(txtOutput);
	
	
	public GUI1_ReadFile(){
		/* set GUI's properties */
		setTitle("");
		setSize(300, 300);
		setLayout(null);
		
		/* set components's properties */
		txtInput.setBounds(10, 10, 150, 25);
		btnBrowse.setBounds(170, 10, 100, 25);
		Scroll.setBounds(10, 40, 260, 150);
		
		add(txtInput);
		add(Scroll);
		add(btnBrowse);
		
	}

	public static void main(String[] args) {
		GUI1_ReadFile GUI = new GUI1_ReadFile();
		GUI.setDefaultCloseOperation(EXIT_ON_CLOSE);
		GUI.setVisible(true);
	}

}
