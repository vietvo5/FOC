import java.awt.Graphics;
import java.util.LinkedList;

import javax.swing.JPanel;


public class Graph_Panel extends JPanel {
	
	LinkedList<Graph_Dot_OBJ> vertices = new LinkedList<Graph_Dot_OBJ>();
	private int [][] edge = null;
	
	public void setGraph(LinkedList <Graph_Dot_OBJ> a, int [][] b){
		this.vertices = a;
		this.edge = b;
	}
	
	public void paint(Graphics g){
		super.paint(g);
	
		if (vertices.size() != 0){
			for (int i = 0; i < vertices.size(); i++) {
				g.drawString(vertices.get(i).getName(), vertices.get(i).getX(), vertices.get(i).getY() - 10);
				g.fillOval(vertices.get(i).getX(), vertices.get(i).getY(), 8, 8);
			}
			
			for (int i = 0; i < edge.length; i ++){
				for (int j = 0; j < edge.length; j ++){
					if (edge[i][j] > 0){
						g.drawLine(vertices.get(i).getX() + 4, vertices.get(i).getY() + 4, vertices.get(j).getX() + 4, vertices.get(j).getY() + 4);
						g.drawString(Integer.toString(edge[i][j]), (vertices.get(i).getX() + vertices.get(j).getX())/2, (vertices.get(i).getY() + vertices.get(j).getY())/2);
					}					
				}
			}
				
		}
		
	}
}
