import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Graph_Display extends JFrame {
	/* ALL COMPONENTS */
	JTextField txtPath1 = new JTextField();
	JTextField txtPath2 = new JTextField();
	JButton btnBrowse1 = new JButton("Browse1");
	JButton btnBrowse2 = new JButton("Browse2");
	JButton btnDraw = new JButton("Draw");
	Graph_Panel Graph = new Graph_Panel();

	/* ALL VARIABLES */
	BufferedReader brFile1 = null, brFile2 = null;
	String sCurrentLine;
	int nVertices = 0;
	LinkedList<Graph_Dot_OBJ> Vertices = new LinkedList<Graph_Dot_OBJ>();
	int[][] Edge = null;

	public Graph_Display() {
		setTitle("");
		setSize(350, 150);
		setLayout(null);

		txtPath1.setBounds(10, 10, 200, 25);
		txtPath2.setBounds(10, 40, 200, 25);
		btnBrowse1.setBounds(220, 10, 100, 25);
		btnBrowse2.setBounds(220, 40, 100, 25);
		btnDraw.setBounds(10, 70, 100, 25);
		txtPath1.setEditable(false);
		txtPath2.setEditable(false);
		btnBrowse2.setEnabled(false);
		btnDraw.setEnabled(false);
		Graph.setBounds(10, 100, 500, 500);
		add(txtPath1);
		add(txtPath2);
		add(btnBrowse1);
		add(btnBrowse2);
		add(btnDraw);
		add(Graph);

		/* ACTION */
		ActionListener btnAction = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JButton btnA = (JButton) arg0.getSource();

				// get name
				String name = btnA.getText();

				if (name.equals(btnBrowse1.getText())) {
					JFileChooser fc = new JFileChooser();
					fc.setMultiSelectionEnabled(false);
					int iChoice = fc.showOpenDialog(null);
					if (iChoice == JFileChooser.APPROVE_OPTION) {
						File fOpen = fc.getSelectedFile();
						txtPath1.setText(fOpen.getAbsolutePath());
					} else {
						return;
					}

					// Enable btnBrowse2
					btnBrowse2.setEnabled(true);
				}

				if (name.equals(btnBrowse2.getText())) {
					JFileChooser fc = new JFileChooser();
					fc.setMultiSelectionEnabled(false);
					int iChoice = fc.showOpenDialog(null);
					if (iChoice == JFileChooser.APPROVE_OPTION) {
						File fOpen = fc.getSelectedFile();
						txtPath2.setText(fOpen.getAbsolutePath());
					} else {
						return;
					}
					
					
					
					// Enable btnDraw
					btnDraw.setEnabled(true);
				}

				if (name.equals(btnDraw.getText())) {
					/* READ THE VERTICES.TXT */
					try {
						brFile1 = new BufferedReader(new FileReader(
								txtPath1.getText()));
						try {
							while ((sCurrentLine = brFile1.readLine()) != null) {

								String[] temp = sCurrentLine.split("\\s+");
								Graph_Dot_OBJ tempDot = new Graph_Dot_OBJ();
								tempDot.setName(temp[0]);
								tempDot.setX(Integer.parseInt(temp[1]));
								tempDot.setY(Integer.parseInt(temp[2]));

								Vertices.add(tempDot);
								nVertices++;
							}
						} catch (IOException e) {
							e.printStackTrace();
							JOptionPane.showMessageDialog(null, "Invalid file");
							return;
						}
					} catch (FileNotFoundException e) {
						e.printStackTrace();
						JOptionPane.showMessageDialog(null, "Invalid file");
						return;
					}

					Edge = new int[nVertices][nVertices];

					/* READ THE EDGE.TXT */
					try {
						brFile2 = new BufferedReader(new FileReader(
								txtPath2.getText()));
						try {
							while ((sCurrentLine = brFile2.readLine()) != null) {

								String[] temp = sCurrentLine.split("\\s+");
								int fVertices = Integer.parseInt(temp[0]) - 1;
								int lVertices = Integer.parseInt(temp[1]) - 1;
								int weight = Integer.parseInt(temp[2]);
								Edge[fVertices][lVertices] = weight;
							}
						} catch (IOException e) {
							e.printStackTrace();
							JOptionPane.showMessageDialog(null, "Invalid file");
							return;
						}
					} catch (FileNotFoundException e) {
						e.printStackTrace();
						JOptionPane.showMessageDialog(null, "Invalid file");
						return;
					}

					/* set other weights to 0 */
					for (int i = 0; i < Edge.length; i++)
						for (int j = 0; j < Edge.length; j++)
							if (Integer.toString(Edge[i][j]).length() == 0)
								Edge[i][j] = 0;

					
					setSize(700, 700);
					Graph.setGraph(Vertices, Edge);
					Graph.repaint();
					
				}

			}
		};

		/* ADD ACTION */
		btnBrowse1.addActionListener(btnAction);
		btnBrowse2.addActionListener(btnAction);
		btnDraw.addActionListener(btnAction);


	}

	public static void main(String[] args) {
		Graph_Display GUI = new Graph_Display();
		GUI.setDefaultCloseOperation(EXIT_ON_CLOSE);
		GUI.setVisible(true);
	}

}
