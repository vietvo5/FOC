import javax.swing.JFrame;


public class Grid_Frame extends JFrame {
	/* ALL COMPONENT */
	Grid_Panel Grid = new Grid_Panel();
	
	public Grid_Frame(){
		setTitle("Grid");
		setSize(500, 500);
		add("Center", Grid);
	}

	public static void main(String[] args) {
		Grid_Frame GUI = new Grid_Frame();
		GUI.setDefaultCloseOperation(EXIT_ON_CLOSE);
		GUI.setVisible(true);
	}

}
