
public class Bubble_Sort {

	public static void main(String[] args) {
		int []b = {3, 4, 6, 1, 10, 20, 3, 5};
		Bubble_Sort(b);
		for (int i = 0; i < b.length; i ++)
			System.out.println(b[i]);

	}

	public static void Bubble_Sort(int[] a ){
		boolean flag = true;
		int tempInt;
		while (flag) {
			flag = false; // set flag to false awaiting a possible swap
			for (int j = 0; j < a.length - 1; j++) {
				if ( a[j] > a[j+1]){
					tempInt = a[j];
					a[j] = a[j+1];
					a[j+1] = tempInt;
					flag = true;
				}
			}

		}

	}
}
