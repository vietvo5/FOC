import java.io.File;

import javax.swing.JFileChooser;


public class OpenDialog {

	public static void main(String[] args) {
		JFileChooser fc = new JFileChooser();
		fc.setMultiSelectionEnabled(false);
		//fc.setFileFilter(new OfficeFilter());
		int iChoice = fc.showOpenDialog(null);
		if (iChoice == JFileChooser.APPROVE_OPTION) {
			File fOpen = fc.getSelectedFile();
			//txtInput.setText(fOpen.getAbsolutePath());
			System.out.println(fOpen.getAbsolutePath());
		} else {
			return;
		}
	}

}
