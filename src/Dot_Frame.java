import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * 
 */

/**
 * @author US
 *
 */
public class Dot_Frame extends JFrame {
	int time = 300;
	/* ALL COMPONENTS */
	String[] colorStrings = { "Red", "Green", "Blue" };
	JComboBox colorList = new JComboBox(colorStrings);
	JSlider Slider = new JSlider(JSlider.HORIZONTAL, 0, 4, 0);
	Dot_Panel PointPanel = new Dot_Panel();
	JPanel Panel2 = new JPanel();

	public Dot_Frame() {
		
		setSize(600, 600);
		add("Center", PointPanel);
		colorList.setSelectedItem(0);
		Panel2.add("Center", colorList);
		Slider.setMinorTickSpacing(1);
		Slider.setMajorTickSpacing(1);
		Slider.setPaintLabels(true);
		Panel2.add("Center", Slider);
		add("South", Panel2);

		/* SLIDER */
		Slider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {
				int SliderValue = Slider.getValue();

				if (SliderValue == 0) {
					time = 400;
				} else if (SliderValue == 1) {
					time = 300;

				} else if (SliderValue == 2) {
					time = 200;

				} else if (SliderValue == 3) {
					time = 100;
				} else if (SliderValue == 4) {
					time = 50;
				}

				PointPanel.tmMoving.stop();
				PointPanel.tmMoving.setDelay(time);
				PointPanel.tmMoving.start();

			}
		});

		/* COLOR LIST */
		colorList.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String a = (String) colorList.getSelectedItem().toString();

				if (a.equals("Red")) {
					PointPanel.setcColor(Color.RED);
				}

				if (a.equals("Blue")) {
					PointPanel.setcColor(Color.BLUE);
				}

				if (a.equals("Green")) {
					PointPanel.setcColor(Color.GREEN);
				}
				PointPanel.repaint();

			}
		});

	}

	public static void main(String[] args) {
		Dot_Frame GUI = new Dot_Frame();
		GUI.setDefaultCloseOperation(EXIT_ON_CLOSE);
		GUI.setVisible(true);

	}

}
