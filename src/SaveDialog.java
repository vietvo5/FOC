import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class SaveDialog {

	public static void main(String[] args) {
		// Variable
		String data = "Hello";
		
		JFileChooser finder = new JFileChooser();
		finder.setFileFilter(new FileNameExtensionFilter("Board Files", "boa"));
		int returnVal = finder.showSaveDialog(null);
		if (returnVal == javax.swing.JFileChooser.APPROVE_OPTION) {
			java.io.File file = finder.getSelectedFile();
			String file_name = file.toString();
			if (!file_name.endsWith(".boa"))
				file_name += ".boa";
			JOptionPane.showMessageDialog(null, file_name);
			FileWriter a = null;
			try {
				a = new FileWriter(new File(file_name));
			} catch (IOException e) {
				e.printStackTrace();
			}

			BufferedWriter b = new BufferedWriter(a);
			try {
				b.write(data);
				b.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}
