import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;


public class Read_File {

	
	public static void main(String[] args) {
		
		/* ALL VARIABLES */
		String Path, sCurrentLine, data = "";
		BufferedReader brReader = null;
		
		/* OPEN DIALOG */
		JFileChooser fc = new JFileChooser();
		fc.setMultiSelectionEnabled(false);
		//fc.setFileFilter(new OfficeFilter());
		int iChoice = fc.showOpenDialog(null);
		if (iChoice == JFileChooser.APPROVE_OPTION) {
			File fOpen = fc.getSelectedFile();
			//txtInput.setText(fOpen.getAbsolutePath());
			Path = fOpen.getAbsolutePath();
			
		} else {
			return;
		}
		
		/* READ FILE */
		try {

			brReader = new BufferedReader(new FileReader(Path));

			while ((sCurrentLine = brReader.readLine()) != null) {
				data += sCurrentLine + "\n";
			}

		} catch (IOException e) {
			// e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Invalid path !");
			return;

		} finally {
			try {
				if (brReader != null)
					brReader.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		/* Print out */
		System.out.println(data);
		
		/* Return Value */
		data = "";
	}

}
