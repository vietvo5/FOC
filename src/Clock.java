import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;

import java.util.Date;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.awt.Graphics;

/**
 * 
 */

/**
 * @author LocMai T142772
 *
 */
public class Clock extends JFrame {
	/**
	 * @param args
	 */
	Timer cTimer = null;
	JLabel clock = new JLabel("");
	// dat format cho dong ho
	DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	// lay thoi gian voi ham Date()
	Date date = new Date();
	Font myFont = new Font("Serif", Font.BOLD, 100);
	int x1 = 200, y1 = 200, xs = 200, ys = 200, xm = 200, ym = 200, xh = 200,
			yh = 200;

	public Clock() {
		// dat title,size va layout
		setTitle("T142772 - Analog Clock");
		setSize(400, 400);
		setLayout(null);
		// set clock truoc khi chay timer
		String[] cmd = dateFormat.format(date).split(":");
		int s = Integer.parseInt(cmd[2]), m = Integer.parseInt(cmd[1]), h = Integer
				.parseInt(cmd[0]);
		xs = (int) (Math.cos(s * 3.14f / 30 - 3.14f / 2) * 120 + x1);
		ys = (int) (Math.sin(s * 3.14f / 30 - 3.14f / 2) * 120 + y1);
		xm = (int) (Math.cos(m * 3.14f / 30 - 3.14f / 2) * 100 + x1);
		ym = (int) (Math.sin(m * 3.14f / 30 - 3.14f / 2) * 100 + y1);
		xh = (int) (Math.cos((h * 30 + m / 2) * 3.14f / 180 - 3.14f / 2) * 80 + x1);
		yh = (int) (Math.sin((h * 30 + m / 2) * 3.14f / 180 - 3.14f / 2) * 80 + y1);
		// /////////////////////////////////////////////////////////////
		// ham timer thay doi bien date va label sau moi 1s
		cTimer = new Timer(1000, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				date = new Date();
				String[] cmd = dateFormat.format(date).split(":");
				int s = Integer.parseInt(cmd[2]), m = Integer.parseInt(cmd[1]), h = Integer
						.parseInt(cmd[0]);
				xs = (int) (Math.cos(s * 3.14f / 30 - 3.14f / 2) * 120 + x1);
				ys = (int) (Math.sin(s * 3.14f / 30 - 3.14f / 2) * 120 + y1);
				xm = (int) (Math.cos(m * 3.14f / 30 - 3.14f / 2) * 100 + x1);
				ym = (int) (Math.sin(m * 3.14f / 30 - 3.14f / 2) * 100 + y1);
				xh = (int) (Math
						.cos((h * 30 + m / 2) * 3.14f / 180 - 3.14f / 2) * 80 + x1);
				yh = (int) (Math
						.sin((h * 30 + m / 2) * 3.14f / 180 - 3.14f / 2) * 80 + y1);
				repaint();
			}
		});
		cTimer.start();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Clock cMain = new Clock();
		cMain.setDefaultCloseOperation(EXIT_ON_CLOSE);
		cMain.setVisible(true);
	}

	public void paint(Graphics g) {
		super.paint(g);
		g.drawLine(x1, y1, xs, ys);
		g.drawLine(x1, y1, xm, ym);
		g.drawLine(x1, y1, xh, yh);
		g.drawOval(x1 - 125, y1 - 120, 250, 250);
		g.drawString("9", x1 - 145, y1 + 0);
		g.drawString("3", x1 + 135, y1 + 0);
		g.drawString("12", x1 - 10, y1 - 130);
		g.drawString("6", x1 - 8, y1 + 145);
		g.drawString("10", x1 - 130, y1 - 60);
		g.drawString("11", x1 - 80, y1 - 115);
		g.drawString("2", x1 + 125, y1 - 55);
		g.drawString("1", x1 + 80, y1 - 110);
		g.drawString("7", x1 - 80, y1 + 130);
		g.drawString("8", x1 - 130, y1 + 75);
		g.drawString("4", x1 + 120, y1 + 70);
		g.drawString("5", x1 + 70, y1 + 130);
	}
}