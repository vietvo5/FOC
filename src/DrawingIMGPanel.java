import javax.imageio.ImageIO;   
import javax.swing.JPanel;
import javax.swing.Timer;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @author Loc Mai T142772
 *
 */
public class DrawingIMGPanel extends JPanel {

	/**
	 * @param args
	 */
	//x la toa do x,w la do rong
	int x=-100,w=400;
	BufferedImage buffImage = null;
	Timer cTimer = null;
	public DrawingIMGPanel() {
		//set size panel 400 400
		setSize(w,w);
		//doc duong dan file hinh
		try {
			buffImage= ImageIO.read(new File("./imgs/Car-08.gif"));
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//timer chinh vi tri cua hinh xe
		cTimer = new Timer(40,new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if (x>w) {
					x=-100;
				}
				x=x+5;
				repaint();
			}
		});
		cTimer.start();
	}
	//paint
	public void paint(Graphics g) {
		super.paint(g);
		g.drawImage(buffImage, x, 120, this);
	}
}
