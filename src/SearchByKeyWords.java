import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * 
 */

/**
 * @author Hoang Viet T143870
 * 
 */
public class SearchByKeyWords extends JFrame {
	// Variables
	String output = "", path = "";
	String sCurrentLine, data = "";
	int nNum = 0, nSearch = 0, count = 0;
	BufferedReader brReader = null;
	Student_OBJ[] student = new Student_OBJ[100];

	/* COMPONENTS */
	JTextField txtInput = new JTextField();
	JTextField txtSearch = new JTextField();
	JButton btnBrowse = new JButton("Browse");
	JButton btnRead = new JButton("Read");
	JButton btnSearch = new JButton("Search");
	JLabel lbla = new JLabel("Key : ");
	JTextArea txtOutput = new JTextArea();
	JScrollPane Scroll = new JScrollPane(txtOutput);

	public SearchByKeyWords() {
		setTitle("Search Student - T143870");
		setSize(400, 350);
		setLayout(null);

		/* LABEL */
		lbla.setBounds(120, 40, 60, 25);
		add(lbla);

		/* TEXT FIELD */
		txtInput.setBounds(10, 10, 250, 25);
		txtSearch.setBounds(160, 40, 100, 25);
		txtInput.setEditable(false);
		txtSearch.setEditable(false);
		add(txtInput);
		add(txtSearch);

		/* BUTTON */

		btnBrowse.setBounds(270, 10, 100, 25);
		btnRead.setBounds(10, 40, 100, 25);
		btnSearch.setBounds(270, 40, 100, 25);

		btnRead.setEnabled(false);
		btnSearch.setEnabled(false);

		add(btnRead);
		add(btnBrowse);
		add(btnSearch);

		/* TEXT AREA */
		Scroll.setBounds(10, 80, 360, 200);
		add(Scroll);

		/* ACTION */

		ActionListener iButton = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				JButton aButton = (JButton) e.getSource();

				if (aButton.getText().equals(btnBrowse.getText())) {
					// Open dialog

					JFileChooser fc = new JFileChooser();
					fc.setMultiSelectionEnabled(false);
					//fc.setFileFilter(new cPro01_OfficeFilter_T143870());
					int iChoice = fc.showOpenDialog(null);
					if (iChoice == JFileChooser.APPROVE_OPTION) {
						File fOpen = fc.getSelectedFile();
						txtInput.setText(fOpen.getAbsolutePath());
					} else {
						return;
					}

					btnRead.setEnabled(true);

				} else if (aButton.getText().equals(btnRead.getText())) {

					// Get path
					path = txtInput.getText();

					// Read txt
					try {

						brReader = new BufferedReader(new FileReader(path));

					} catch (IOException e1) {
						// e.printStackTrace();
						JOptionPane.showMessageDialog(null, "File not found !");
						return;

					}

					// Read txt
					try {
						while ((sCurrentLine = brReader.readLine()) != null) {
							String[] temp = sCurrentLine.trim().split(",");

							try {
								// Create student
								student[nNum] = new Student_OBJ();

								// Save data
								student[nNum].set_ID(temp[0]);
								student[nNum].set_FName(temp[1]);
								student[nNum].set_LName(temp[2]);
								nNum++;
							} catch (Exception e2) {
								JOptionPane.showMessageDialog(null,
										"Invalid format !");
								nNum = 0;
								return;
							}

						}
					} catch (IOException e2) {
						return;
					}

					// print student data
					for (int i = 0; i < nNum; i++) {
						output += student[i].get_ID() + " "
								+ student[i].get_FName() + " "
								+ student[i].get_LName() + "\n";
					}

					txtOutput.setText(output);
					// return value
					data = "";
					output = "";
					nSearch = nNum;
					nNum = 0;

					// Enable Button
					btnSearch.setEnabled(true);
					txtSearch.setEditable(true);
					return;
				} else if (aButton.getText().equals(btnSearch.getText())) {
					String temp = txtSearch.getText();
					boolean find = false;

					for (int i = 0; i < nSearch; i++) {

						if (student[i].get_ID().equals(temp)
								|| student[i].get_FName().equals(temp)
								|| student[i].get_LName().equals(temp)) {
							count++;
							output += student[i].get_ID() + " "
									+ student[i].get_FName() + " "
									+ student[i].get_LName() + "\n";
							find = true;

						}

					}

					// print
					if (find == true) {
						output += "Found : " + Integer.toString(count) + "\n";
					} else {
						// add Count number
						output = "Not found";
					}
					txtOutput.setText(output);
					output = "";
					count = 0;
					return;
				}
			}
		};

		/* SET ACTION */
		btnBrowse.addActionListener(iButton);
		btnRead.addActionListener(iButton);
		btnSearch.addActionListener(iButton);

	}

	public static void main(String[] args) {
		SearchByKeyWords GUI = new SearchByKeyWords();
		GUI.setDefaultCloseOperation(EXIT_ON_CLOSE);
		GUI.setVisible(true);
	}

}
