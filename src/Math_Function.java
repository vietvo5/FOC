import java.text.NumberFormat;

/**
 * 
 */

/**
 * @author Hoang Viet T143870
 * 
 */
public class Math_Function {
	// Factorial function

	public static double Factorial(double n) {
		double f = 1;

		for (int i = 1; i <= n; i++) {
			f *= i;
		}

		return f;
	}

	// Exponential function

	public static double Exponential(double n, double ex) {
		double e = 1;

		if (ex >= 0) {
			for (int i = 1; i <= ex; i++) {
				e *= n;
			}

		} else if (ex < 0) {
			for (int i = 1; i <= -ex; i++) {
				e *= 1 / n;
			}
		}

		return e;

	}

	// Abs function
	public static double Abs(double n) {
		if (n >= 0) {
			return n;
		} else {
			return (-1.0 * n);
		}
	}

	// Count the number
	public static int Count(double n) {
		int k = 0;
		String a = Integer.toString((int) n);
		for (int i = 0; i < a.length(); i++) {
			k++;
		}
		return k;
	}

	public static double Square(double n, double me) // me : 1 -> 8
	{
		int count = Count(n), limit = 30;

		double x0 = 0, x1 = 0;
		x0 = count * Exponential(10.0, 2.0);
		for (int i = 1; i <= limit; i++) {
			x1 = (0.5) * (x0 + (n / x0));
			if (Abs(x1 - x0) > Exponential(10, -(me * 1.0))) {
				x0 = x1;

			} else {
				return x1;
			}
			// new value of x0;
		}
		return x1;

	}

	// Check hex
	public static boolean isHex(String str) {
		boolean re = true;
		String hex[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A",
				"B", "C", "D", "E", "F" };
		String temp[] = str.split("");

		for (int i = 1; i < temp.length; i++) {
			if (re == true) {

				for (int k = 0; k < hex.length; k++) {
					if (temp[i].equals(hex[k])) {
						re = true;
						break;
					} else {
						re = false;
					}
				}
			} else {
				break;
			}
		}

		return re;
	}

	// Convert Hex to bin
	public static String HexToBin(String str) {
		// Output
		String output = "";

		// Bin
		String bin[] = { "0000", "0001", "0010", "0011", "0100", "0101",
				"0110", "0111", "1000", "1001", "1010", "1011", "1100", "1101",
				"1110", "1111", };

		// Bin
		String hex[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A",
				"B", "C", "D", "E", "F" };

		String temp[] = str.split("");

		for (int i = 1; i < temp.length; i++) {
			for (int k = 0; k < bin.length; k++) {
				if (temp[i].equals(hex[k])) {
					output += bin[k];
				}
			}
		}

		return output;
	}
	
	// Check Oct
	public static boolean isOct(String str) {
		boolean re = true;
		String oct[] = { "0", "1", "2", "3", "4", "5", "6", "7"};
		String temp[] = str.split("");

		for (int i = 1; i < temp.length; i++) {
			if (re == true) {

				for (int k = 0; k < oct.length; k++) {
					if (temp[i].equals(oct[k])) {
						re = true;
						break;
					} else {
						re = false;
					}
				}
			} else {
				break;
			}
		}

		return re;
	}

	// Convert Oct to bin
	public static String OctToBin(String str) {
		// Output
		String output = "";

		// Oct
		String bin[] = { "000", "001", "010", "011", "100", "101", "110", "111" };

		// Bin
		String oct[] = { "0", "1", "2", "3", "4", "5", "6", "7" };

		String temp[] = str.split("");

		for (int i = 1; i < temp.length; i++) {
			for (int k = 0; k < oct.length; k++) {
				if (temp[i].equals(oct[k])) {
					output += bin[k];
				}
			}
		}

		return output;
	}

	public static String getFractionDigits(double x, int comma) {
		String s;
		NumberFormat nf = NumberFormat.getInstance(); // get instance
		nf.setMaximumFractionDigits(comma); // set decimal places
		return s = nf.format(x);
	}
	
	public static String DecToBin(int number) {
		return DecToBin(number, "");
	}
	
	public static String DecToBin(int number, String bin) {
	    if (number >= 1)
	        return DecToBin(number / 2,Integer.toString(number % 2) +  bin);
	    else
	        return bin;

	}
}
