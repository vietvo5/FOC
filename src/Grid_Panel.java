import java.awt.Graphics;

import javax.swing.JPanel;


public class Grid_Panel extends JPanel {
	private int n = 20, d = 20, w = n*d;
	public void paint(Graphics g){
		super.paint(g);
		g.drawRect(0, 0, w, w);
		for (int i = 0; i < w; i+= d){
			g.drawLine(i, 0, i, w); 
			g.drawLine(0, i, w, i);
		}
	}
}
