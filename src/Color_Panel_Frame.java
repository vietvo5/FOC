import javax.swing.JFrame;
import java.awt.Color;

/**
 * 
 */

/**
 * @author Hoang Viet T143870
 * 
 */
public class Color_Panel_Frame extends JFrame {
	
	/* ALL COMPONENTS */
	ColorPanel ColorPanel = new ColorPanel();
	
	public Color_Panel_Frame(){
		setTitle("T143870 - TestDrawing");
		setSize(700, 600);
		add("Center", ColorPanel);
		
		
	}

	public static void main(String[] args) {
		Color_Panel_Frame GUI = new Color_Panel_Frame();
		GUI.setDefaultCloseOperation(EXIT_ON_CLOSE);
		GUI.setVisible(true);
	}

}
