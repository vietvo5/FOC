import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

public class ColorPanel extends JPanel {
	private int n = 25, md = 20;
	private int wPanel = 100, hPanel = 200, d = wPanel / 2,
			xGrid = wPanel + 20, width = wPanel + 20 + n * md, height = n * md;
	private int x, y, xFill = 0, yFill = 0;
	private Color cColor = Color.BLUE, bColor = cColor;
	private boolean check = false;

	public Color getbColor() {
		return bColor;
	}

	public void setbColor(Color bColor) {
		this.bColor = bColor;
	}

	public Color getcColor() {
		return cColor;
	}

	public void setcColor(Color cColor) {
		this.cColor = cColor;
	}

	public ColorPanel() {
		this.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent arg0) {

			}

			@Override
			public void mousePressed(MouseEvent arg0) {

			}

			@Override
			public void mouseExited(MouseEvent arg0) {

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {

			}

			@Override
			public void mouseClicked(MouseEvent arg0) {
				x = arg0.getX();
				y = arg0.getY();

				if ((x >= 0) && (x <= wPanel / 2) && (y >= 0)
						&& (y <= hPanel / 4)) {
					setcColor(Color.BLUE);
					repaint();
				}

				if ((x >= wPanel / 2) && (x <= wPanel) && (y >= 0)
						&& (y <= hPanel / 4)) {
					setcColor(Color.RED);
					repaint();
				}

				if ((x >= 0) && (x <= wPanel / 2) && (y >= hPanel / 4)
						&& (y <= hPanel / 2)) {
					setcColor(Color.YELLOW);
					repaint();
				}

				if ((x >= wPanel / 2) && (x <= wPanel) && (y >= hPanel / 4)
						&& (y <= hPanel / 2)) {
					setcColor(Color.GREEN);
					repaint();
				}

				if ((x >= xGrid) && (x <= width) && (y >= 0) && (y <= height)) {
					setbColor(getcColor());
					xFill = xGrid + ((x - xGrid) / md) * md; // (x - xGrid)/md
																// return x
																// position of
																// square
					yFill = (y / md) * md; // y/md return y position of square
					repaint();
				}

			}
		});
	}

	public void paint(Graphics g) {
		super.paint(g);
		g.fillRect(0, 0, wPanel, hPanel);
		g.drawRect(0, 0, wPanel, hPanel);
		g.drawLine(wPanel / 2, 0, wPanel / 2, hPanel / 2);
		g.drawLine(0, hPanel / 4, wPanel, hPanel / 4);
		g.drawLine(0, hPanel / 2, wPanel, hPanel / 2);
		g.drawLine(0, 3 * hPanel / 4, wPanel, 3 * hPanel / 4);

		g.setColor(Color.BLUE);
		g.fillRect(0, 0, d, d);

		g.setColor(Color.RED);
		g.fillRect(wPanel / 2, 0, d, d);

		g.setColor(Color.YELLOW);
		g.fillRect(0, hPanel / 4, d, d);

		g.setColor(Color.GREEN);
		g.fillRect(wPanel / 2, hPanel / 4, d, d);

		g.setColor(Color.WHITE);
		g.fillRect(0, hPanel / 2, 2 * d, d);

		g.setColor(getcColor());
		g.fillRect(0, 3 * hPanel / 4, 2 * d, d);

		// Paint grid
		g.setColor(Color.BLACK);
		g.drawRect(xGrid, 0, n * md, n * md);
		for (int i = xGrid; i <= (xGrid + md * n); i += md) {
			g.drawLine(i, 1, i, md * n);
		}

		for (int i = 0; i < md * n; i += md) {
			g.drawLine(xGrid, i, xGrid + md * n, i);
		}

		// Fill square
		if (xFill != 0 && yFill != 0) {
			g.setColor(getbColor());
			g.fillRect(xFill + 1, yFill + 1, md - 1, md - 1);

		}
	}
}
