
public class Check_Leap_Year {
	public static boolean CheckYear(String dob){
		int d, m, y;
		String [] b = null;
		
		/* DOB */
		try {
			b = dob.split("/");
		} catch (Exception e1) {
			return false;
		}

		// Convert
		try {
			d = Integer.parseInt(b[0]);
			m = Integer.parseInt(b[1]);
			y = Integer.parseInt(b[2]);

		} catch (Exception e) {
			return false;
		}

		if (d <= 0 || d > 31) {
			return false;
		}

		if (m <= 0 || m > 12) {
			return false;
		}

		// Nam nhuan
		if ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0) {
			if (m == 2 && d == 29){
				return true;
			}
		} else {
			if (m == 2 && d == 29)
			{
				return false;
			}
		}

		return true;
	}
}
