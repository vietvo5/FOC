/**
 * 
 */

/**
 * @author Hoang Viet T1439870
 *
 */
public class Student_OBJ {
	private String id, fname, lname;
	
	public Student_OBJ(){
		id = "";
		fname = "";
		lname = "";
	}
	
	public Student_OBJ(String a, String b, String c){
		id = a;
		fname = b;
		lname = c;
	}
	
	/* ID */
	public void set_ID(String a){
		id = a;
	}
	
	public String get_ID(){
		return id;
	}
	
	/* FNAME */
	public void set_FName(String a){
		fname = a;
	}
	
	public String get_FName(){
		return fname;
	}
	
	/* LNAME */
	public void set_LName(String a){
		lname = a;
	}
	
	public String get_LName(){
		return lname;
	}
	
}
