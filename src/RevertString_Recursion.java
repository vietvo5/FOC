import java.util.Scanner;


public class RevertString_Recursion {

	public static void main(String[] args) {
		Scanner data = new Scanner(System.in);
		
		System.out.println("Nhap vao String : ");
		String n = data.nextLine();
		
		
		System.out.println("Result : " + RevertString(n));
		
		
	}
	
	public static String RevertString(String s){
		
		if (s.length() <= 1)
			return s;
		else {
			int n = s.length();
			return (s.substring(n - 1, n) + RevertString(s.substring(0, n - 1)));
		}
			
	}

}
